<?php


namespace MyNamespace\Sizecharts\Model;

use MyNamespace\Sizecharts\Api\Data\SizechartsInterface;

/**
 * Class Sizecharts
 * 
 * Creates sizecharts
 * 
 * @category MyNamespace
 * @package  MyNamespace_Sizecharts
 *
 */
class Sizecharts extends \Magento\Framework\Model\AbstractModel 
                 implements SizechartsInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('MyNamespace\Sizecharts\Model\ResourceModel\Sizecharts');
    }

    /**
     * Get sizecharts_id
     * @return string
     */
    public function getSizechartsId()
    {
        return $this->getData(self::SIZECHARTS_ID);
    }

    /**
     * Set sizecharts_id
     * @param string $sizechartsId
     * @return \MyNamespace\Sizecharts\Api\Data\SizechartsInterface
     */
    public function setSizechartsId($sizechartsId)
    {
        return $this->setData(self::SIZECHARTS_ID, $sizechartsId);
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getSizechartsId();
    }

    /**
     * Set id
     * @param string $id
     * @return \MyNamespace\Sizecharts\Api\Data\SizechartsInterface
     */
    public function setId($id)
    {
        return $this->setSizechartsId($id);
    }

    /**
     * Get sizecharts_label
     * @return string
     */
    public function getSizechartsLabel()
    {
        return $this->getData(self::SIZECHARTS_LABEL);
    }

    /**
     * Set sizecharts_label
     * @param string $sizecharts_label
     * @return \MyNamespace\Sizecharts\Api\Data\SizechartsInterface
     */
    public function setSizechartsLabel($sizecharts_label)
    {
        return $this->setData(self::SIZECHARTS_LABEL, $sizecharts_label);
    }

    /**
     * Get is_unisex
     * @return string
     */
    public function getIsUnisex()
    {
        return $this->getData(self::IS_UNISEX);
    }

    /**
     * Set is_unisex
     * @param string $is_unisex
     * @return \MyNamespace\Sizecharts\Api\Data\SizechartsInterface
     */
    public function setIsUnisex($is_unisex)
    {
        return $this->setData(self::IS_UNISEX, $is_unisex);
    }

    /**
     * Get sizechart_active_from
     * @return string
     */
    public function getSizechartActiveFrom()
    {
        return $this->getData(self::SIZECHART_ACTIVE_FROM);
    }

    /**
     * Set sizechart_active_from
     * @param string $sizechart_active_from
     * @return \MyNamespace\Sizecharts\Api\Data\SizechartsInterface
     */
    public function setSizechartActiveFrom($sizechart_active_from)
    {
        return $this->setData(self::SIZECHART_ACTIVE_FROM, $sizechart_active_from);
    }
}
